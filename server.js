var express = require('express');
var path = require('path')
var cors = require('cors')

var port = process.env.PORT || 3000
var app = express();
app.use(cors())

app.use(express.static('public'));
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, './public', 'resume.html'));
});


app.listen(port);